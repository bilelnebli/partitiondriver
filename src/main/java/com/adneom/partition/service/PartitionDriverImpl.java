package com.adneom.partition.service;

import com.adneom.partition.exception.PartitionDriverException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/***
 * @author binebli
 * 
 * This class in the implementation of the partition driver which contains two static methods responsible for 
 * partitioning a collection/array into a collection/array of sub-collections/sub-arrays 
 * according to a partition unit.
 * 
 */
public class PartitionDriverImpl {
	/***
	 * This static method is used to partition a collection into a collection of sub-collections
	 * using a partition unit.
	 * @param <T> 
	 * @param initialCollection
	 * @param partitionUnit
	 * @return list of Sublists
	 * @throws PartitionDriverException
	 */
	public static <T> List<List<T>> partitionCollection(Collection<T> initialCollection, int partitionUnit)
			throws PartitionDriverException {
		checkInput(initialCollection, partitionUnit);
		List<List<T>> partitionnedCollection = new ArrayList<>();
		int i = 0;
		for (T initialElement : initialCollection) {
			if (i % partitionUnit == 0) {
				partitionnedCollection.add(new ArrayList<>());
			}
			partitionnedCollection.get(partitionnedCollection.size() - 1).add(initialElement);
			i++;
		}
		return partitionnedCollection;

	}
	/**
	 * This static method is used to partition an array into an array of sub-arrays
	 * using a partition unit.
	 * @param <T>
	 * @param initialCollection
	 * @param partitionUnit
	 * @return array of SubArrays
	 * @throws PartitionDriverException
	 */
	public static <T> List<List<T>> partitionCollection(T[] initialCollection, int partitionUnit)
			throws PartitionDriverException {
		checkInput(Arrays.asList(initialCollection), partitionUnit);
		return partitionCollection(Arrays.asList(initialCollection), partitionUnit);
	}
	/***
	 * This method checks the input values and throw an exception where wrong values are introduced
	 * @param <T>
	 * @param initialCollection
	 * @param partitionUnit
	 * @throws PartitionDriverException
	 */
	private static <T> void checkInput(Collection<T> initialCollection, int partitionUnit)
			throws PartitionDriverException {
		if (initialCollection.isEmpty())
			throw new PartitionDriverException("The input collection is Empty");
		if (partitionUnit < 1) {
			throw new PartitionDriverException("The partition unit is lower that 1");
		}
	}
}
