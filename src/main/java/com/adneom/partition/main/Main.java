package com.adneom.partition.main;

import com.adneom.partition.exception.PartitionDriverException;
import com.adneom.partition.service.PartitionDriverImpl;

import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;
import static java.util.stream.Collectors.toList;

public class Main {
	private static final Random random = new Random();
	private static final int LIST_SIZE = 50;

	private static final List<Integer> list = IntStream.range(0, LIST_SIZE + random.nextInt(50)).boxed()
			.collect(toList());

	public static void main(String[] args) throws PartitionDriverException {
		int partitionUnit = random.nextInt(20);
		System.out.println("Initial " + list.size() + " elements' List: " + list);
		System.out.println("Partition Unit: " + partitionUnit);

		System.out.println("Result: " + PartitionDriverImpl.partitionCollection(list, partitionUnit));
		System.out.println();
		System.out.println("Initial Array: " + list);
		System.out.println("Partition Unit: " + 2);
		String[] array = { "A", "B", "C", "D", "E", "F" };
		System.out.println("Result: " +PartitionDriverImpl.partitionCollection(array, 2));
	}
}
