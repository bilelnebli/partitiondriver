package com.adneom.partition.exception;
/***
 * 
 * @author binebli
 * This class is responsible for managing the bad input arguments.
 */
public class PartitionDriverException extends Exception {
    private static final long serialVersionUID = 1L;
	public PartitionDriverException(){super();}
    public PartitionDriverException(String exp){super(exp);}


}
