package test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import com.adneom.partition.exception.PartitionDriverException;
import com.adneom.partition.service.PartitionDriverImpl;

public class PartitionTest {
	
	@Test
	public void partitionStringCollection() throws PartitionDriverException {
		 List<String> stringsList = Arrays.asList("AB", "CD", "EF", "GH", "IJ", "KL", "MN", "OP");

	        Set<String> stringsSet = new HashSet<>();
	        stringsSet.add("AB");
	        stringsSet.add("CD");
	        stringsSet.add("EF");
	        stringsSet.add("GH");
	        List<List<String>> twoElementsSet = new ArrayList<>();
	        twoElementsSet.add(Arrays.asList("AB", "CD"));
	        twoElementsSet.add(Arrays.asList("EF", "GH"));

	        List<List<String>> oneElementList = new ArrayList<>();
	        oneElementList.add(Arrays.asList("AB"));
	        oneElementList.add(Arrays.asList("CD"));
	        oneElementList.add(Arrays.asList("EF"));
	        oneElementList.add(Arrays.asList("GH"));
	        oneElementList.add(Arrays.asList("IJ"));
	        oneElementList.add(Arrays.asList("KL"));
	        oneElementList.add(Arrays.asList("MN"));
	        oneElementList.add(Arrays.asList("OP"));

	        List<List<String>> twoElementsList = new ArrayList<>();
	        twoElementsList.add(Arrays.asList("AB", "CD"));
	        twoElementsList.add(Arrays.asList("EF", "GH"));
	        twoElementsList.add(Arrays.asList("IJ", "KL"));
	        twoElementsList.add(Arrays.asList("MN", "OP"));

	        List<List<String>> threeElementsList = new ArrayList<>();
	        threeElementsList.add(Arrays.asList("AB", "CD", "EF"));
	        threeElementsList.add(Arrays.asList("GH", "IJ", "KL"));
	        threeElementsList.add(Arrays.asList("MN", "OP"));

	        assertTrue(areEquals(oneElementList, PartitionDriverImpl.partitionCollection(stringsList, 1)));
	        assertTrue(areEquals(twoElementsList, PartitionDriverImpl.partitionCollection(stringsList, 2)));
	        assertTrue(areEquals(threeElementsList, PartitionDriverImpl.partitionCollection(stringsList, 3)));
	
	        assertTrue(areEquals(twoElementsSet, PartitionDriverImpl.partitionCollection(stringsSet, 2)));

	}
	
	@Test
	public void partitionIntegerCollection() throws PartitionDriverException {
		List<Integer> integersList = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8,9,10);

        List<List<Integer>> oneElementList = new ArrayList<>();
        oneElementList.add(Arrays.asList(1));
        oneElementList.add(Arrays.asList(2));
        oneElementList.add(Arrays.asList(3));
        oneElementList.add(Arrays.asList(4));
        oneElementList.add(Arrays.asList(5));
        oneElementList.add(Arrays.asList(6));
        oneElementList.add(Arrays.asList(7));
        oneElementList.add(Arrays.asList(8));
        oneElementList.add(Arrays.asList(9));
        oneElementList.add(Arrays.asList(10));


        List<List<Integer>> twoElementsList = new ArrayList<>();
        twoElementsList.add(Arrays.asList(1,2));
        twoElementsList.add(Arrays.asList(3,4));
        twoElementsList.add(Arrays.asList(5,6));
        twoElementsList.add(Arrays.asList(7,8));
        twoElementsList.add(Arrays.asList(9,10));
        
        List<List<Integer>> threeElementsList = new ArrayList<>();
        threeElementsList.add(Arrays.asList(1, 2, 3));
        threeElementsList.add(Arrays.asList(4, 5, 6));
        threeElementsList.add(Arrays.asList(7, 8, 9));
        threeElementsList.add(Arrays.asList(10));


        Set<Integer> integersSet = new HashSet<>();
        integersSet.add(1);
        integersSet.add(2);
        integersSet.add(3);
        integersSet.add(4);
        List<List<Integer>> twoElementsSet = new ArrayList<>();
        twoElementsSet.add(Arrays.asList(1,2));
        twoElementsSet.add(Arrays.asList(3,4));

        assertTrue(areEquals(oneElementList, PartitionDriverImpl.partitionCollection(integersList, 1)));
        assertTrue(areEquals(twoElementsList, PartitionDriverImpl.partitionCollection(integersList, 2)));
        assertTrue(areEquals(threeElementsList, PartitionDriverImpl.partitionCollection(integersList, 3)));
        
        assertTrue(areEquals(twoElementsSet, PartitionDriverImpl.partitionCollection(integersSet, 2)));

	}
	
	@Test
    public void partitionIntegerArray() throws  PartitionDriverException{
        Integer[] integersArray = {1, 2, 3, 4, 5, 6};

        List<List<Integer>> firstChunkWay = new ArrayList<>();
        firstChunkWay.add(Arrays.asList(1));
        firstChunkWay.add(Arrays.asList(2));
        firstChunkWay.add(Arrays.asList(3));
        firstChunkWay.add(Arrays.asList(4));
        firstChunkWay.add(Arrays.asList(5));
        firstChunkWay.add(Arrays.asList(6));

        List<List<Integer>> secondChunkWay = new ArrayList<>();
        secondChunkWay.add(Arrays.asList(1, 2));
        secondChunkWay.add(Arrays.asList(3, 4));
        secondChunkWay.add(Arrays.asList(5, 6));

        List<List<Integer>> thirdChunkWay = new ArrayList<>();
        thirdChunkWay.add(Arrays.asList(1, 2, 3));
        thirdChunkWay.add(Arrays.asList(4, 5, 6));

        assertTrue(areEquals(firstChunkWay, PartitionDriverImpl.partitionCollection(integersArray, 1)));
        assertTrue(areEquals(secondChunkWay, PartitionDriverImpl.partitionCollection(integersArray, 2)));
        assertTrue(areEquals(thirdChunkWay, PartitionDriverImpl.partitionCollection(integersArray, 3)));
    }
	
	@Test(expected = PartitionDriverException.class)
    public void emptyList() throws PartitionDriverException {
        List<Integer> emptyList = new ArrayList<>();
        PartitionDriverImpl.partitionCollection(emptyList, 1);
        fail("The collection should not be Empty");
    }
	 @Test(expected = PartitionDriverException.class)
	    public void invalidPartitionUnit() throws PartitionDriverException{
	        List<Integer> list = Arrays.asList(1,2,3);
	        PartitionDriverImpl.partitionCollection(list, -50);
	        fail("The partition unit must be equal or greater to 1");
	    }
	private <T> boolean areEquals(List<List<T>> expectedList, List<List<T>> inputList) {
        return expectedList.equals(inputList);
    }
}
